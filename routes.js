const express = require('express');
const routes = express.Router();

const login = require('./middleware/login');

const CustomerController = require('./controllers/CustomerController');
const FavoritesController = require('./controllers/FavoritesController');
const LoginController = require('./controllers/LoginController');
const ServiceController = require('./controllers/ServiceController');
const WorkerController = require('./controllers/WorkerController');
const WorkerServiceTypesController = require('./controllers/WorkerServiceTypesController');
const CategoriesController = require('./controllers/CategoriesController');
const { Router } = require('express');

//Customer 
routes.get('/customers', CustomerController.index)
      .get('/customer/:id', CustomerController.list)
      .post('/newCustomer', CustomerController.store)
      .delete('/rmvCustomer/:id', CustomerController.destroy);


//Favorites
routes.get('/favorites', FavoritesController.index)
      .get('/userFavorites/:id', FavoritesController.showByUserType)
      .post('/addFavorite', FavoritesController.store)
      .post('/updateUserFavoritesCount/:id', FavoritesController.updateFavoritesCount)
      .delete('/rvmFavorite/:id', FavoritesController.destroy)

//Login
routes.post('/login', LoginController.login)

//Service
routes.get('/services', ServiceController.index)
      .get('/service/:id',ServiceController.show)
      .get('/workerServices/:id',ServiceController.list)
      .post('/newService', ServiceController.store)
      .post('/updateUserServiceCount/:id', ServiceController.updateServicesCount)
      .delete('/rmvService/:id', ServiceController.destroy)

//Worker
routes.get('/workers', WorkerController.index)
      .get('/worker/:id', WorkerController.show)
      .get('/workersC', WorkerController.showC)
      .get('/workersN', WorkerController.showN)
      .post('/newWorker', WorkerController.store)
      .delete('/rmvWorker/:id',WorkerController.destroy)
      .put('/addLike/:id', WorkerController.likesUpdate)

//WorkerServiceTypesController
routes.get('/workerServicesTypes', WorkerServiceTypesController.list)
      .get('/workerServicesTypesCategory',WorkerServiceTypesController.index)
      .get('/workerServicesTypesCategoryC',WorkerServiceTypesController.indexCityState)
      .get('/workerServicesTypesCategoryN',WorkerServiceTypesController.indexNeighborhood)
      .post('/indexCityStateForApp', WorkerServiceTypesController.indexCityStateForApp)
      .post('/indexNeighborhoodForApp', WorkerServiceTypesController.indexNeighborhoodForApp)
      .post('/newCatToWorker', WorkerServiceTypesController.store)
      .delete('/rmvWorkerCat/:id',WorkerServiceTypesController.destroy)

//Categories
routes.get('/categories', CategoriesController.index)
      .get('/category/:id', CategoriesController.show)

module.exports = routes;