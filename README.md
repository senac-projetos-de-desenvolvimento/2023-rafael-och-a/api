# API

A API implementada para este projeto se baseia em **MySQL** em conjunto com **Node JS**. Ao utilizar o node, também fazemos uso do **Knex**, **Express** e **Cors**.

## Instalação

Primeiro, devemos instalar o MySQL 

https://dev.mysql.com/downloads/workbench/

Também é necessário o NodeJS

https://nodejs.org/en/download

Em seguida, é necessário baixar ou clonar este repositório, em seguida instalar os pacotes definidos no package.json

    npm i

Com estas etapas realizadas, agora será necessário criar um branco no nosso MySQL workbench com as seguintes configurações

    Nome do banco  :: salvaobra
    Senha do banco :: 201160
    nome do user   :: root

Com o banco criado e as dependências do projeto instaladas, nos basta apenas subir as **migrações** que geram as tebelas e rodar a **seed** que irá popular uma tabela específica.

Para rodar as **migrações** basta utilizar 

    knex migrate:latest

Para popular a tabela de categorias com as **seeds**

    knex seed:run

Com todas estas etapas realizadas, nos basta agora apenas rodar nossa API. Para isto, basta apenas utilizar

    node app

