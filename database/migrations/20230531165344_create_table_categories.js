/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('categories', (table) => {
        table.increments();
        table.string('name', 50).unique().notNullable();
        table.string('description', 150).notNullable();
        // created_at e updated_at
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.down = (knex) => knex.schema.dropTable('categories');
