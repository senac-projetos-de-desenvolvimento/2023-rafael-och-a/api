/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('service', (table) => {
        table.increments();
        table.string('name', 100).notNullable();
        table.string('description',255).notNullable();
        table.decimal('price', 9.2).notNullable();
        table.boolean('isProvided').notNullable();
        
       //Rel.Categories
       table.integer('category_id').notNullable().unsigned();
       table.foreign('category_id')
       .references('categories.id')
       .onDelete('restrict')
       .onUpdate('cascade');        
       
       //Rel. Worker
        table.integer('worker_id').notNullable().unsigned();
        table.foreign('worker_id')
            .references('worker.id')
            .onDelete('restrict')
            .onUpdate('cascade');
            
        // created_at e updated_at
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => knex.schema.dropTable('service');
