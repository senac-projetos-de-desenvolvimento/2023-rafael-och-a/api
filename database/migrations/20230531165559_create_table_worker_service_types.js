/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('workerServiceTypes', (table) => {
       table.increments();
    
       //Rel.Categories
       table.integer('category_id').notNullable().unsigned();
       table.foreign('category_id')
       .references('categories.id')
       .onDelete('restrict')
       .onUpdate('cascade');        
       
       //Rel. Worker
        table.integer('worker_id').notNullable().unsigned();
        table.foreign('worker_id')
            .references('worker.id')
            .onDelete('restrict')
            .onUpdate('cascade');

        // created_at e updated_at
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => knex.schema.dropTable('workerServiceTypes');