/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('worker', (table) => {
        table.increments();
        table.string('password', 100).notNullable();
        table.string('title', 100).notNullable();
        table.string('email', 120).unique().notNullable();
        table.string('phoneA', 11).notNullable();
        table.string('phoneB', 11).notNullable();
        table.string('cnpj', 14);
        table.string('cpf',11);
        //Etc-info
        table.integer('likes').notNullable();
        table.integer('favorites').notNullable();
        table.integer('services').notNullable();
        table.string('description').notNullable();
        table.string('avatar');

        //Location
        table.string('cep', 8).notNullable();
        table.string('adress', 100).notNullable();
        table.string('neighborhood', 100).notNullable();
        table.string('city').notNullable();
        table.string('state', 60).notNullable();

        //'Userype flag'
        table.boolean('isWorker').notNullable();

        // created_at e updated_at
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.down = (knex) => knex.schema.dropTable('worker');