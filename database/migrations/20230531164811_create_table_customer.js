/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('customer', (table) => {
        table.increments();
        table.string('firstName', 60).notNullable();
        table.string('lastName', 120).notNullable();
        table.string('password', 60).notNullable();
        table.string('phone', 11).notNullable();
        table.string('email', 100).unique().notNullable();
        //Etc-info
        table.integer('favorites').notNullable();
        table.integer('services').notNullable();
        //Location
        table.string('cep', 8).notNullable();
        table.string('adress', 100).notNullable();
        table.string('neighborhood', 100).notNullable();
        table.string('city', 100).notNullable();
        table.string('state', 60).notNullable();
        table.string('avatar');
        //'Userype flag'
        table.boolean('isWorker').notNullable();


        // created_at e updated_at
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.down = (knex) => knex.schema.dropTable('customer');