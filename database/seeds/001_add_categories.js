
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('categories').del()
    .then(function () {
      // Inserts seed entries
      return knex('categories').insert([
        { name: 'pedreiro', description:'Pedreiros e mestres de obra' },
        { name: 'servente', description:'Ajudantes para obras em geral' },
        { name: 'arquitetura', description:'Arquitetos credenciados' },
        { name: 'engenharia', description:'Engenheiros credenciados' },
        { name: 'encanamento', description:'Servicos hidraulicos em geral' },
        { name: 'eletricista', description:'Servicos eletricos em geral' },
        { name: 'marcenaria', description:'Servicos em madeira e moveis planejados' },
        { name: 'frete', description:'Fretes e mudanças' },
        { name: 'demolicao', description:'Demolicao e containers' },
        { name: 'favorito', description:'Prestadores favoritos' },
      ]);
    });
};
