module.exports = {
    development: {
        client: 'mysql2',
        connection: {
            host: '127.0.0.1',
            database: 'salvaobra',
            user: 'root',
            password: '201160'
        },
        migrations: {
            tableName: 'migrations',
            directory: 'database/migrations'
        },
        seeds: {
            directory: 'database/seeds'
        }
    }
};