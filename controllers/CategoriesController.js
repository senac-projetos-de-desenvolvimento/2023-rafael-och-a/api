const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

// << To-do >>
// - Adicionar o PUT (Update) para um worker
// - Integrar o JWT

module.exports = {
    //Index :: Listagem de todos os Prestadores / Workers
    async index(req, res) {
        const categories = await knex('categories');
        res.status(200).json(categories);
    },

    //List :: Lista o prestador de um determinado id
    async show(req, res){
        const id = req.params.id;
  
        try{
            const data = await knex('categories').where({id});
            res.status(200).json(data);

        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    }
}