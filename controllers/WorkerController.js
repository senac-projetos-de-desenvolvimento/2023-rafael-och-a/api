const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

// << To-do >>
// - Adicionar o PUT (Update) para um worker
// - Integrar o JWT

module.exports = {
    //Index :: Listagem de todos os Prestadores / Workers
    async index(req, res) {
        const workers = await knex('worker');
        res.status(200).json(workers);
    },

    //List :: Lista o prestador de um determinado id
    async show(req, res){
        const id = req.params.id;
  
        try{
            const data = await knex('worker').where({id});
            res.status(200).json(data[0]);

        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //List <Neighborhood> :: Retorna uma lista com todos os prestadores 
    //de um determinado bairro
    async showN(req, res){
        const {neighborhood, city, state} = req.body;

        try{
            const data = await knex('worker').where({neighborhood, city, state}).orderBy('likes','desc');;
            res.status(200).json(data);
        }catch(error){
            res.status(400).json({ erro: error.message });
        }
    },

    //List <City> ::  Retorna uma lista com todos os prestadores de 
    //uma determinada cidade
    async showC(req, res){
        const {city, state} = req.body;

        try{
            const data = await knex('worker').where({city, state}).orderBy('likes','desc');
            res.status(200).json(data);
        }catch(error){
            res.status(400).json({ erro: error.message });
        }
    },
    //Store :: Criação de um novo Prestador/Worker
    async store(req, res){
        const isWorker = true;

        const {
            password,
            title,
            email,
            phoneA,
            phoneB,
            cnpj,
            cpf,
            likes,
            favorites,
            services,
            description,
            avatar,
            cep,
            adress,
            neighborhood,
            city,
            state
        } = req.body;

        //Verifica se algum input não foi recebido
        if(!password || !title || !email || !phoneA || !description ||
           !cep || !adress || !neighborhood || !city || !state  ){
            res.status(400).json({ erro: 'Parâmetros insuficientes!' });
            return;
        }

        //Verificações de E-MAIL :: Worker && Customer (não se repete, indiferente do tipo de usuário)
        try{
            const data = await knex('worker').where({ email });
            if (data.length) {
                res.status(400).json({ erro: 'Este e-mail já se encontra cadastrado.' });
                return;
            }
            else{
                try{
                    const data = await knex('customer').where({ email });
                    if (data.length) {
                        res.status(400).json({ erro: 'Este e-mail já se encontra cadastrado.' });
                        return;
                    }
                }catch (error) {
                    res.status(400).json({ erro: error.message });
                    return;
                }
            }
        }catch (error) {
            res.status(400).json({ erro: error.message });
            return;
        }

        //Registra
        const hash = bcrypt.hashSync(password, 10);

        try {
            const newWorker = await knex('worker').insert({  
                password: hash,
                title,
                email,
                phoneA,
                phoneB,
                cnpj,
                cpf,
                likes:0,
                favorites:0,
                services:0,
                description,
                avatar,
                cep,
                adress,
                neighborhood,
                city,
                state,
                isWorker
            });
            res.status(201).json({ id: newWorker[0] });
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    async likesUpdate(req, res){
        const id = req.params.id;

        try{
            const data = await knex('worker').where({id}).update({'likes': knex.raw('likes + 1')});
            res.status(200).json(data);
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Destroy :: Remoção de um Prestador
    async destroy(req, res){
        const id = req.params.id;

        try{
            const isRemoved = await knex('worker').where('id', id).del();
            if(isRemoved)
                res.status(200).json({ msg: 'Remoção realizada com sucesso!' });
            else
                res.status(404).json({msg: 'Prestador não encontrado!'});
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }

    }
}