const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

// << To-do >>
// - Adicionar o PUT (Update) para um customer

module.exports = {
    //Index :: Listagem de Clientes / Customers ----------------------------------
    async index(req, res){
        const customers = await knex('customer');
        res.status(200).json(customers);
    },

    //List :: Lista o prestador de um determinado id
    async list(req, res){
        const id = req.params.id;
  
        try{
            const data = await knex('customer').where({id});
            if(data.length <= 0 )
                res.status(404).json({error:'No Customer finded'});
            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Store :: Criação de um novo Cliente/Customer ----------------------------------
    async store(req, res){
        const isWorker = false;

        const{
            firstName,
            lastName,
            password,
            phone,
            email,
            favorites,
            services,
            cep,
            adress,
            neighborhood,
            city,
            state,
            avatar
        } = req.body;

        //Verifica se algum input não foi recebido
        if(!firstName || !lastName || !password || !phone || !email ||
            !cep || !adress || !neighborhood || !city || !state ||
            !avatar){
             res.status(400).json({ erro: 'Parâmetros insuficientes' });
             return;
         }

        //Verifica se o e-mail já não se encontra cadastrado, seja em worker, seja em customer
        try{
            const data = await knex('worker').where({ email });
            if (data.length) {
                res.status(400).json({ erro: 'Este e-mail já se encontra cadastrado.' });
                return;
            }
            else{
                try{
                    const data = await knex('customer').where({ email });
                if (data.length) {
                    res.status(400).json({ erro: 'Este e-mail já se encontra cadastrado.' });
                    return;
            }
                }catch (error) {
                    res.status(400).json({ erro: error.message });
                    return;
                }
            }
        }catch (error) {
            res.status(400).json({ erro: error.message });
            return;
        }

        //Registra
         const hash = bcrypt.hashSync(password, 10);

         try {
             const newCustomer = await knex('customer').insert({  
                password: hash,
                firstName,
                lastName,
                phone,
                email,
                favorites:0,
                services:0,
                cep,
                adress,
                neighborhood,
                city,
                state,
                avatar,
                isWorker
             });
             res.status(201).json({ id: newCustomer[0] });
         } catch (error) {
             res.status(400).json({ erro: error.message });
         }
    },

    //Destroy :: Remoção de um Cliente / Customer
    async destroy(req, res){
        const id = req.params.id;

        try{
            const isRemoved = await knex('customer').where('id', id).del();
            if(isRemoved)
                res.status(200).json({ msg: 'Remoção realizada com sucesso!' });
            else
                res.status(404).json({msg: 'Cliente não encontrado!'});
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }

    }
}