const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

//Teoria
//Devemos verificar se primeiro o e-mail do cliente, depois o do prestador

module.exports = {
    async login(req, res) {
        const { email, password } = req.body;

        let customerData;
        let workerData;

        //Verificações::Checa se todos os dados foram passados
        if (!email || !password) {
            res.status(400).json({ erro: "Login e/ou password inválidos" });
            return;
        }

        //Customer
        try{
            customerData = await knex("customer").where({ email });
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }

        //Worker
        try{
            workerData = await knex("worker").where({email});
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }

        //Verificação de validade do Cliente / Customer
        if(customerData.length > 0 && bcrypt.compareSync(password, customerData[0].password)){
            const token = jwt.sign(
                {
                    user_id: customerData[0].id,
                    user_name: customerData[0].firstName,
                    user_lastName: customerData[0].lastName,
                    user_email: customerData[0].email,
                    user_phone: customerData[0].phone,
                    //Location
                    user_cep: customerData[0].cep,
                    user_adress: customerData[0].adress,
                    user_neighborhood: customerData[0].neighborhood,
                    user_city: customerData[0].city,
                    user_state: customerData[0].state,
                    //user_type
                    use_isWorker: customerData[0].isWorker
                },
                //Utilizamos o env para uma camada a mais de proteção
                process.env.JWT_KEY,
                { expiresIn: "1h" }
            );
            //res.status(200).json({ token });
            res.status(200).json(customerData[0]);
        }

        //Verificação de validade do Prestador / Worker
        else if(workerData.length > 0 && bcrypt.compareSync(password, workerData[0].password)){
            const token = jwt.sign(
                {
                    user_id: workerData[0].id,
                    user_name: workerData[0].title,
                    user_email: workerData[0].email,
                    user_phone: workerData[0].phoneA,
                    user_likes: workerData[0].likes,
                    user_favorited: workerData[0].favorited,
                    user_description: workerData[0].description,
                    //Location
                    user_cep: workerData[0].cep,
                    user_adress: workerData[0].adress,
                    user_neighborhood: workerData[0].neighborhood,
                    user_city: workerData[0].city,
                    user_state: workerData[0].state,
                    use_isWorker: workerData[0].isWorker
                },
                //Utilizamos o env para uma camada a mais de proteção
                process.env.JWT_KEY,
                { expiresIn: "1h" }
            );
            //res.status(200).json({ token });
            res.status(200).json(workerData[0]);
        }

        //Caso não exista nenhum registro contendo as informações recebidas
        else{
            res.status(400).json({ erro: "Login e/ou senha inválidos" });
            return;
        }


    }
}