const knex = require("../database/dbConfig");

module.exports = {
    //Index :: Listagem de todos os favoritos
    async index(req, res) {
        const favorites = await knex
        .select(
            'f.id',
            'f.user_id',
            'f.worker_id',
            'f.isWorker',
            'w.title as worker_Title'
        )
        .from('favorites as f')
        .join('worker as w', 'w.id', 'f.worker_id');

        res.status(200).json(favorites);
    },

    //Lista todos os favorios de um determinado usuário
    async show(req, res){
        const user_id = req.params.id;
        const {isWorker} = req.body;
        try{

            const data = await knex
            .select(
                'worker.title',
                'worker.adress',
                'worker.phoneA',
                'worker.likes'
            )
            .from('worker')
            .where('favorites.user_id', user_id)
            .join('favorites')

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Lista todos os favorios de um usuário
    async showByUserType(req, res){
        const user_id = req.params.id;
        const {isWorker} = req.body;
        try{
            let data;
            if(isWorker === true) {
                data = await knex
                .select(
                    'f.id',
                    'f.user_id',
                    'f.worker_id',
                    'f.isWorker',
                    'w.title as worker_Title'
                )
                .from('favorites as f', 'f.user_id', user_id)
                .join('worker as w', 'w.id', 'f.worker_id')
                .where('f.isWorker', isWorker)
            }
            else {
                data = await knex
                .select(
                    'f.id',
                    'f.user_id',
                    'f.worker_id',
                    'f.isWorker',
                    'w.title as worker_Title'
                )
                .from('favorites as f', 'f.user_id', user_id)
                .join('worker as w', 'w.id', 'f.worker_id')
                .join('customer as c', 'c.id', 'f.user_id')
                .where('f.isWorker', isWorker)
            }

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Store :: Registro de um novo favorito
    async store(req, res){
        const {user_id, worker_id, isWorker = false} = req.body;

        //Verifica se algum input não foi recebido
        if(!user_id || !worker_id ){
            res.status(400).json({ erro: 'Parâmetros insuficientes' });
            return;
        }

        try{
            const newFavorite = await knex('favorites').insert({user_id, worker_id, isWorker});
            res.status(201).json({ id: newFavorite[0] });
        }catch (error) {
            res.status(400).json({ erro: error.message });
            return;
        }
    },

    //Incrementa o campo 'favorites' de um
    //usuário específico em 1
    async updateFavoritesCount(req, res){
        const id = req.params.id;
        const {isWorker} = req.body;

        try{
            const data = await knex(isWorker ? 'worker' : 'customer')
            .where({id}).update({'favorites': knex.raw('favorites + 1')});

            if(data <= 0)
                res.status(404).json({msg:'Usuário não encontrado...', data: data});
            else
                res.status(200).json({msg:'Favorites atualizado com sucesso!', data:data});
        }catch(error){
            res.status(400).json({erro: error.message});
        }
    },

    //Destroy :: Remoção de um Favorito
    async destroy(req, res){
        const id = req.params.id;

        try{
            const isRemoved = await knex('favorites').where('id', id).del();
            if(isRemoved)
                res.status(200).json({ msg: 'Remoção realizada com sucesso!' });
            else
                res.status(404).json({msg: 'Favorito não encontrado!'});
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }

    }
}