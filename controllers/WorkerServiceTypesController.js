const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

module.exports = {
    //List :: Listagem geral 
    async list(req, res){
        const data = await knex
        .select(
            'wst.id',
            'w.id as w_id',
            'w.title as w_title',
            'w.adress',
            'w.phoneA',
            'w.likes',
            'w.neighborhood',
            'w.city',
            'w.state',
            'cat.id as c_id',
            'cat.name as c_name',
            'cat.description'
        )
        .from('worker as w')
        .join('workerServiceTypes as wst', 'wst.worker_id', 'w.id')
        .join('categories as cat', 'wst.category_id', 'cat.id');

        res.status(200).json(data);
    },

    //Listagem Específica :: 
    //  - Retorna todos os prestadores específicos de uma determinada categoria
    async index(req, res){
        const {category_id} = req.body;

        try{
            const data = await knex
            .select(
                'wst.id',
                'w.id as w_id',
                'w.title as w_title',
                'w.adress',
                'w.phoneA',
                'w.likes',
                'w.neighborhood',
                'w.city',
                'w.state',
                'cat.id as c_id',
                'cat.name as c_name',
                'cat.description'                
                )
            .from('worker as w')
            .join('workerServiceTypes as wst', 'w.id','=','wst.worker_id')
            .join('categories as cat', 'cat.id','=','wst.category_id')
            .where('wst.category_id', category_id);
            
            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },


    //Listagem Específica :: 
    //  - Retorna todos os prestadores específicos de uma determinada categoria
    //  - Retorna somente aqueles que estão na mesma cidade e estado que o usuário
    async indexCityState(req, res){
        const {category_id,city, state} = req.body;

        try{
            const data = await knex
            .select(
                'wst.id',
                'w.id as w_id',
                'w.title as w_title',
                'w.adress',
                'w.phoneA',
                'w.likes',
                'w.neighborhood',
                'w.city',
                'w.state',
                'cat.id as c_id',
                'cat.name as c_name',
                'cat.description'                
                )
            .from('worker as w')
            .join('workerServiceTypes as wst', 'w.id','=','wst.worker_id')
            .join('categories as cat', 'cat.id','=','wst.category_id')
            .where('wst.category_id', category_id)
            .where('w.city', city)
            .where('w.state', state);            

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Listagem Específica :: 
    //  - Retorna todos os prestadores específicos de uma determinada categoria
    //  - Retorna somente aqueles que estão na mesma cidade e estado que o usuário
    //  - Retorna apenas aqueles que além da cidade/estado, também se encontram 
    // no mesmo BAIRRO do usuário.
    async indexNeighborhood(req, res){
        const {
            category_id,
            city,
            state,
            neighborhood
        } = req.body;

        try{
            const data = await knex
            .select(
                'wst.id',
                'w.id as w_id',
                'w.title as w_title',
                'w.adress',
                'w.phoneA',
                'w.likes',
                'w.neighborhood',
                'w.city',
                'w.state',
                'cat.id as c_id',
                'cat.name as c_name',
                'cat.description'                
                )
            .from('worker as w')
            .join('workerServiceTypes as wst', 'w.id','=','wst.worker_id')
            .join('categories as cat', 'cat.id','=','wst.category_id')
            .where('wst.category_id', category_id)
            .where('w.city', city)
            .where('w.state', state)
            .where('w.neighborhood', neighborhood);

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },


    //Listagens para o consumo no APP
    //É igual aos métodos anteriores, mas somente com o retorno
    
    //Cat + City + State
    async indexCityStateForApp(req, res){
        const {category_id,city, state} = req.body;

        try{
            const data = await knex
            .select(
                'w.id',
                'w.password',
                'w.title',
                'w.email',
                'w.phoneA',
                'w.phoneB',
                'w.cnpj',
                'w.cpf',
                'w.likes',
                'w.favorites',
                'w.services',
                'w.description',
                'w.avatar',
                'w.cep',
                'w.adress',
                'w.neighborhood',
                'w.city',
                'w.state',
                'w.isWorker',
            )
            .from('worker as w')
            .join('workerServiceTypes as wst', 'w.id','=','wst.worker_id')
            .join('categories as cat', 'cat.id','=','wst.category_id')
            .where('wst.category_id', category_id)
            .where('w.city', city)
            .where('w.state', state);            

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Cat + City + State + Neighborhood
    async indexNeighborhoodForApp(req, res){
        const {
            category_id,
            city,
            state,
            neighborhood
        } = req.body;

        try{
            const data = await knex
            .select(
                'w.id',
                'w.password',
                'w.title',
                'w.email',
                'w.phoneA',
                'w.phoneB',
                'w.cnpj',
                'w.cpf',
                'w.likes',
                'w.favorites',
                'w.services',
                'w.description',
                'w.avatar',
                'w.cep',
                'w.adress',
                'w.neighborhood',
                'w.city',
                'w.state',
                'w.isWorker',       
            )
            .from('worker as w')
            .join('workerServiceTypes as wst', 'w.id','=','wst.worker_id')
            .join('categories as cat', 'cat.id','=','wst.category_id')
            .where('wst.category_id', category_id)
            .where('w.city', city)
            .where('w.state', state)
            .where('w.neighborhood', neighborhood);

            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },


    //Store :: Registro de uma nova categoria a um determinado prestador
    async store(req, res){
        const { category_id, worker_id } = req.body;

        //Verificação 
        if(!category_id || !worker_id ){
             res.status(400).json({ erro: 'Parâmetros insuficientes' });
             return;
        }

        try {
            const newData = await knex('workerServiceTypes').insert({  
                category_id,
                worker_id
            });
            res.status(201).json({ id: newData[0] });
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Destroy :: Remoção 
    async destroy(req, res){
        const id = req.params.id;

        try{
            const isRemoved = await knex('workerServiceTypes as wst')
            .where('wst.id', id)
            .del();

            if(isRemoved)
                res.status(200).json({ msg: 'Remoção realizada com sucesso!' });
            else
                res.status(404).json({msg: 'Não encontrado!'});
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    }
}