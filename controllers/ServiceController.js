const knex = require("../database/dbConfig");
const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

module.exports = {
    async index(req, res){
        try{
            // const data = await knex('service');
            const data = await knex
            .select(
                's.id as service_id',
                's.name as service_title',
                'c.name as category_name',
                'w.title as worker_title',
                'w.phoneA as worker_phone',
                's.price',
                's.isProvided',
                's.created_at as register_date'
            )
            .from('service as s')
            .join('worker as w', 'w.id', 's.worker_id')
            .join('categories as c', 'c.id', 's.category_id');
            
            res.status(200).json(data);
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Show :: Retorna um serviço específico a partir de seu id
    async show(req, res) {
        const id = req.params.id;
        
        try{
            const data = await knex('service').where({id});
            res.status(200).json(data);
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Incrementa o campo 'services' de um
    //usuário específico em 1
    async updateServicesCount(req, res){
        const id = req.params.id;
        const {isWorker} = req.body;

        try{
            const data = await knex(isWorker ? 'worker' : 'customer')
            .where({id}).update({'services': knex.raw('services + 1')});

            if(data <= 0)
                res.status(404).json({msg:'Usuário não encontrado...', data: data});
            else
                res.status(200).json({msg:'Services atualizado com sucesso!', data:data});
        }catch(error){
            res.status(400).json({erro: error.message});
        }
    },

    //List :: Lista todos os serviços de um determinado 
    //worker / prestador

    async list(req, res){
        const workerId = req.params.id;

        try{
            const data = await knex.select(
                's.id',
                's.name',
                's.description',
                's.price',
                's.isProvided',
                's.category_id',
                's.worker_id',
                's.created_at as creationDate',
                'c.name as category'
            ).from('service as s')
            .join('categories as c', 'c.id', 's.category_id')
            .where('s.worker_id', workerId);
            if (data.length > 0)
                res.status(200).json(data);
            else
                res.status(404).json({ msg: `Nenhum elemento com ID ${workerId} encontrado` });
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Store :: Cria um novo serviço
    async store(req, res){
        const {
            name,
            description,
            price,
            isProvided,
            category_id,
            worker_id,
        } = req.body;

        if(!category_id || !worker_id || !name || !price || !description 
            ){
             res.status(400).json({ erro: 'Parâmetros insuficientes' });
             return;
        }

        //Verificar se o Worker existe
        try{
            const wData = await knex('worker').where('id', worker_id);
            if( wData.length <= 0 )
                res.status(404).json({ erro: 'Prestador não encontrado' });
        }catch(error) {
            res.status(400).json({ erro: error.message });
        }
        //Verificar se a Category existe
        try{
            const cData = await knex('categories').where('id', category_id);
            if( cData.length <= 0 )
            res.status(404).json({ erro: 'Categoria não encontrada' });
        }catch(error) {
            res.status(400).json({ erro: error.message });
        }

        try{
            const data = await knex('service').insert({
                name,
                description,
                price,
                isProvided,
                category_id,
                worker_id
            });
            res.status(200).json({msg: "Serviço registrado com sucesso!"});
        }catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },

    //Destroy::Remove o elemento do DB
    async destroy(req, res) {
        const id = req.params.id;

        try {
            const data = await knex('service').where('id', id).del();
            
            if(data <= 0 ) 
                res.status(200).json({ msg: `Serviço de (ID:${id} ) não encontrado!` });
            else
                res.status(200).json({ msg: `Serviço de (ID:${id} ) removido com sucesso!` });
        } catch (error) {
            res.status(400).json({ erro: error.message });
        }
    },
}

