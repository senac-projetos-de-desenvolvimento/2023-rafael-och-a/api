const jwt = require("jsonwebtoken");
require('dotenv').config();

//Esta implementação de um middleware de teste foi implementada por mim
//porém não conheço muito sobre, de forma que deve haver uma forma mais 
//eficiente de se fazer isto
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decode = jwt.verify(token, process.env.JWT_KEY);
        //console.log(decode);
        req.user_id = decode.user_id;
        if (decode.user_type == process.env.CLB_TYPE) {
            res.locals.user_id = decode.user_id;
            next();
        }
        else
            return res.status(403).send({ error: "Access forbidden" });
    } catch (error) {
        return res.status(401).send({ error: `Error on authentication: ${error.message}` });
    }
}